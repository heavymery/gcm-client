'use strict';

/**
 * @fileoverview
 * GCM client for Google Cloud Messaging (GCM) Cloud Connection Server (CCS).
 */

//------------------------------------------------------------------------------
//
//  Modules
//
//------------------------------------------------------------------------------

var util = require('util');
var EventEmitter = require('events').EventEmitter;

var uuid = require('node-uuid');
var extend = require('extend');

var Promise = require('promise');

var XMPPClient = require('node-xmpp-client');
var Stanza = require ('node-xmpp-core').Stanza;

var debug = require('debug')('gcm-client');

//------------------------------------------------------------------------------
//
//  Constructor & Properties
//
//------------------------------------------------------------------------------

/**
 * Creates a GCMClient instance.
 *
 * @constructor
 * @param {Object} options
 * @param {string} options.senderId - GCM sender ID
 * @param {string} options.apiKey - API key
 * @param {boolean} [options.isProduction=false] - When testing functionality,
 * connect to `gcm-preprod.googleapis.com:5236` instead of `gcm.googleapis.com:5235`.
 */
function GCMClient(options) {
  EventEmitter.call(this);

  /**
   * Options
   * @property
   */
  this.options = {
    isProduction: false
  };

  if (options) {
    extend(this.options, options);
  }

  debug('options: %s', JSON.stringify(this.options));

  if(!this.options.senderId || !this.options.apiKey) {
    var error = new Error('Missing required option. (senderId, apiKey)');
    debug('error: %s', error);
    this.emit('error', error);
    return;
  }

  /**
   * Is GCM connection draining or not.
   * @property
   */
  this.isGCMDraining = false;

  /**
   * Queued messages for send to GCM.
   * @property
   */
  this.messageQueue = [];

  /**
   * Promises of sended messages to GCM.
   * @property
   */
  this.sendMessagePromise = {};

  var xmppClientOptions = {
    type: 'client',
    jid: this.options.senderId + '@gcm.googleapis.com',
    password: this.options.apiKey,
    port: this.options.isProduction ? 5235 : 5236,
    host: this.options.isProduction ? 'gcm.googleapis.com' : 'gcm-preprod.googleapis.com',
    legacySSL: true,
    preferredSaslMechanism: 'PLAIN'
  };

  debug('XMPP client options: %s', JSON.stringify(xmppClientOptions));

  this.xmppClient = new XMPPClient(xmppClientOptions);

  this.xmppClient.connection.socket.setTimeout(0);
  this.xmppClient.connection.socket.setKeepAlive(true, 10000);

  this.xmppClient.on('online', this._xmppClient_onOnline.bind(this));

  this.xmppClient.on('offline', this._xmppClient_onOffline.bind(this));

  this.xmppClient.on('error', this._xmppClient_onError.bind(this));

  this.xmppClient.on('stanza', this._xmppClient_onStanza.bind(this));
}

util.inherits(GCMClient, EventEmitter);

//------------------------------------------------------------------------------
//
//  Methods
//
//------------------------------------------------------------------------------

/**
 * Sending a downstream message.
 *
 * @method
 * @param {string} to - This parameter specifies the recipient of a message.
 * The value must be a registration token or notification key.
 * @param {Object} data - This parameter specifies the key-value pairs of the message's payload.
 * The key should not be a reserved word ("from" or any word starting with "google" or "gcm").
 * Do not use any of the words defined in this table (such as collapse_key).
 * @param {Object} options - This parameter specifies the options. Refer to table
 * (https://developers.google.com/cloud-messaging/server-ref#send-downstream)
 */
GCMClient.prototype.sendMessage = function(to, data, options) {
  var messageId = uuid.v1();

  var messageData = {
    to: to,
    message_id: messageId,
    data: data
  };

  if(options) {
    Object.keys(options).forEach(function(key) {
			messageData[key] = options[key];
		});
  }

  var message = new Stanza.Element('message')
    .c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify(messageData));

  var promise = new Promise(function(resolve, reject) {
    this.sendMessagePromise[messageId] = {
      'resolve': resolve,
      'reject': reject
    };
  }.bind(this));

  if(this.isGCMDraining) {
    debug('message queued: %s', message);
    this.messageQueue.push(message);
  } else {
    debug('send message: %s', message);
    this.xmppClient.send(message);
  }

  return promise;
};

//------------------------------------------------------------------------------
//
//  Event handlers
//
//------------------------------------------------------------------------------

/**
 * Handler for XMPP Client online event.
 * @private
 */
GCMClient.prototype._xmppClient_onOnline = function(data) {
  debug('online: %s', JSON.stringify(data));
  this.emit('connect', data);

  if(this.isGCMDraining) {
    this.isGCMDraining = false;

    // Send messages in queue
    debug('send queued messages(%s)', this.messageQueue.length);
    while(this.messageQueue.length > 0) {
      var message = this.messageQueue.shift();
      this.xmppClient.send(message);
    }
  }
};

/**
 * Handler for XMPP Client offline event.
 * @private
 */
GCMClient.prototype._xmppClient_onOffline = function() {
  debug('offline(GCM draining: %s)', this.isGCMDraining);

  if(this.isGCMDraining) {
    this.emit('reconnecting');
    this.xmppClient.connect();
  } else {
    this.emit('end');
  }
};

/**
 * Handler for XMPP Client error event.
 * @private
 */
GCMClient.prototype._xmppClient_onError = function(error) {
  debug('error: %s', error);
  this.emit('error', error);
};

/**
 * Handler for XMPP Client stanza event.
 * @private
 */
GCMClient.prototype._xmppClient_onStanza = function(stanza) {
  debug('stanza: %s', stanza);

  if (stanza.is('message')) {
    var messageData = JSON.parse(stanza.getChildText('gcm'));
    if (!messageData) { return; }

    switch (messageData.message_type) {

      // Interpreting a downstream XMPP message response - Succeed
      case 'ack':
        debug('ack: %s', JSON.stringify(messageData));

        // from: This parameter specifies who sent this response. The value is the registration token of the client app.
        // message_id: This parameter uniquely identifies a message in an XMPP connection. The value is a string that uniquely identifies the associated message.
        // registration_id: This parameter specifies the canonical registration token for the client app that the message was processed and sent to. Sender should replace the registration token with this value on future requests; otherwise, the messages might be rejected.

        if(this.sendMessagePromise[messageData.message_id]) {
          this.sendMessagePromise[messageData.message_id].resolve(messageData);
          this.sendMessagePromise[messageData.message_id] = null;
        }
        break;

      // Interpreting a downstream XMPP message response - Failed
      case 'nack':
        debug('nack: %s', JSON.stringify(messageData));

        // from: This parameter specifies who sent this response. The value is the registration token of the client app.
        // message_id: This parameter uniquely identifies a message in an XMPP connection. The value is a string that uniquely identifies the associated message.
        // registration_id: This parameter specifies the canonical registration token for the client app that the message was processed and sent to. Sender should replace the registration token with this value on future requests; otherwise, the messages might be rejected.
        // error: This parameter specifies an error related to the downstream message. It is set when the message_type is nack. See table 11 for details. (https://developers.google.com/cloud-messaging/server-ref#table11)
        // error_description: This parameter provides descriptive information for the error. It is set when the message_type is nack.

        if(this.sendMessagePromise[messageData.message_id]) {
          this.sendMessagePromise[messageData.message_id].reject(messageData);
          this.sendMessagePromise[messageData.message_id] = null;
        }
        break;

      // Cloud Connection Server Messages (XMPP) - Control
      case 'control':
        debug('control: %s', JSON.stringify(messageData));

        // control_type: This parameter specifies the type of control message sent from CCS. Currently, only CONNECTION_DRAINING is supported. XMPP (CCS) sends this control message before it closes a connection to perform load balancing. As the connection drains, no more messages are allowed to be sent to the connection, but existing messages in the pipeline will continue to be processed.

        // Currently, only CONNECTION_DRAINING is supported.
        if(messageData.control_type === 'CONNECTION_DRAINING') {
          this.isGCMDraining = true;
          debug('GCM connection draining');
        }
        break;

      // Cloud Connection Server Messages (XMPP) - Delivery Receipt
      case 'receipt':
        debug('receipt: %s', JSON.stringify(messageData.data));

        // from: This parameter is set to gcm.googleapis.com, indicating that the message is sent from CCS.
        // message_id: This parameter specifies the original message ID that the receipt is intended for, prefixed with dr2: to indicate that the message is a delivery receipt. An app server must send an ack message with this message ID to acknowledge that it received this delivery receipt. See table 9 for the 'ack' message format.
        // category: This parameter specifies the application package name of the client app that receives the message that this delivery receipt is reporting. This is available when message_type is receipt.
        // data: This parameter specifies the key-value pairs for the delivery receipt message. This is available when the message_type is receipt.
        // data.message_status: This parameter specifies the status of the receipt message. It is set to MESSAGE_SENT_TO_DEVICE to indicate the device has confirmed its receipt of the original message.
        // data.original_message_id: This parameter specifies the ID of the original message that the app server sent to the client app.
        // data.device_registration_id: This parameter specifies the registration token of the client app to which the original message was sent.

        // Send an ACK back
        var ackBack = new Stanza.Element('message')
          .c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
            // This parameter specifies the recipient of a response message.
            // The value must be a registration token of the client app that sent the upstream message.
            to: messageData.from,
            // This parameter specifies which message the response is intended for.
            // The value must be the message_id value from the corresponding upstream message.
            message_id: messageData.message_id,
            // This parameter specifies an ack message from an app server to CCS.
            // For upstream messages, it should always be set to ack.
            message_type: 'ack'
          }));

        this.xmppClient.send(ackBack);

        this.emit('receipt', messageData.data);
        break;

      // Interpreting an upstream XMPP message
      default:
        debug('message: %s', JSON.stringify(messageData));

        // from: This parameter specifies who sent the message. The value is the registration token of the client app.
        // category: This parameter specifies the application package name of the client app that sent the message.
        // message_id: This parameter specifies the unique ID of the message.
        // data: This parameter specifies the key-value pairs of the message's payload.

        // Send an ACK back
        var ackBack = new Stanza.Element('message')
          .c('gcm', { xmlns: 'google:mobile:data' }).t(JSON.stringify({
            // This parameter specifies the recipient of a response message. The value must be a registration token of the client app that sent the upstream message.
            to: messageData.from,
            // This parameter specifies which message the response is intended for. The value must be the message_id value from the corresponding upstream message.
            message_id: messageData.message_id,
            // This parameter specifies an ack message from an app server to CCS. For upstream messages, it should always be set to ack.
            message_type: 'ack'
          }));

        this.xmppClient.send(ackBack);

        this.emit('message', messageData);
        break;

    }
  }
};


module.exports = GCMClient;
