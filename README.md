# GCM Client

GCM client for Google Cloud Messaging (GCM) Cloud Connection Server (CCS).

## Install

```sh
$ npm install git+ssh://git@bitbucket.org:jeprojects/gcm-client.git
```

or

```js
// package.json
"dependencies": {
  "gcm-client": "git+ssh://git@bitbucket.org:jeprojects/gcm-client.git"
}
```

## Usage

```js
var GCMClient = require('gcm-client');
```

### Connect to GCM

```js
var gcmClient = new GCMClient({
  senderId: "<project-number>",
  apiKey: "<api-key>",
  isProduction: true // or false
});

// If `isProduction` is true, signalling server will connect to `gcm.googleapis.com:5235`.
// Otherwise connect to `gcm-preprod.googleapis.com:5236`.
// (refer to https://developers.google.com/cloud-messaging/ccs)

gcmClient.on('connect', function() {
  console.log('connected to GCM');
});

gcmClient.on('reconnecting', function() {
  console.log('reconnecting to GCM');
});

gcmClient.on('close', function() {
  console.log('disconnected from GCM');
});

gcmClient.on('error', function(error) {
  console.log('error: %s', error);
});

gcmClient.on('message', function(messageData) {
  console.log('message: %s', JSON.stringify(messageData));
});

gcmClient.on('receipt', function(receiptData) {
  console.log('receipt: %s', JSON.stringify(receiptData));
});
```

### Sending a downstream message

```js
/**
 * Sending a downstream message.
 * @param {string} to - This parameter specifies the recipient of a message.
 * The value must be a registration token or notification key.
 * @param {Object} data - This parameter specifies the key-value pairs of the message's payload.
 * The key should not be a reserved word ("from" or any word starting with "google" or "gcm").
 * Do not use any of the words defined in this table (such as collapse_key).
 * @param {Object} options - This parameter specifies the options. Refer to table
 * (https://developers.google.com/cloud-messaging/server-ref#send-downstream)
 */
gcmClient.sendMessage(to, data, options).then(function(result) {
  if(result.registration_id) {
    // This parameter specifies the canonical registration token for the client app that the message was processed and sent to.
    // TODO: Sender should replace the registration token with this value on future requests; otherwise, the messages might be rejected.
  }
}, function(error) {
  console.log('send message error: %s', error);

  if(error.registration_id) {
    // This parameter specifies the canonical registration token for the client app that the message was processed and sent to.
    // TODO: Sender should replace the registration token with this value on future requests; otherwise, the messages might be rejected.
  }
});
```

### Debug

You can set the DEBUG env to `gcm-client` to print debug info.

```sh
$ DEBUG=gcm-client node app.js
```
